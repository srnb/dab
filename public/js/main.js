/* global $ */

// Get the table element
let getTable = function() { return $("#gametable") }

// ======== VARIABLES ========

var gameWidth = 3
var gameHeight = 3
var cNumPlayers = 2
var currentPlayer = 0
var gameState = []

let playerColors = [
    "#ff0000",
    "#0000ff",
    "#008800",
    "#ffff00",
    "#880088",
    "#ff5500",
    "#00ffff"
]

// ====== END VARIABLES ======

let newTableElement = function(w, h, c) {
    let elem = $('<td></td>')
    elem.addClass(c).width(w).height(h).css("border-radius", Math.min(w, h))
    return elem
}

let newPoint = function(s) {
    return newTableElement(s, s, "dab-point")
}

let newHorizLine = function(l, s) {
    return newTableElement(l, s, "dab-horiz")
}

let newVertiLine = function(l, s) {
    return newTableElement(s, l, "dab-verti")
}

let newSpace = function(l) {
    return newTableElement(l, l, "dab-space")
}

let createPointRow = function(row, width, ps, pl) {
    let tr = $('<tr></tr>')
    // For every column of the row
    for (var col = 0; col < width; col++) {
        let mc = col
        // Add them to the row
        tr.append(newPoint(ps))
        tr.append(newHorizLine(pl, ps).click(function(e) {
            handleClick(true, row, mc)
        }).hover(function(e) {
            if (!hasBeenPicked(true, row, mc)) $(this).css("background", playerColor(currentPlayer))
        }, function(e) {
            if (!hasBeenPicked(true, row, mc)) $(this).css("background", "#ffffff")
        }))
    }
    // Add the ending corner point
    tr.append(newPoint(ps))
    return tr
}

let createSpaceRow = function(row, width, ps, pl) {
    let tr = $('<tr></tr>')
    // Create the second half of a game row
    for (var col = 0; col < width; col++) {
        let mc = col
        // Add them to the row
        tr.append(newVertiLine(pl, ps).click(function(e) {
            handleClick(false, row, mc)
        }).hover(function(e) {
            if (!hasBeenPicked(false, row, mc)) $(this).css("background", playerColor(currentPlayer))
        }, function(e) {
            if (!hasBeenPicked(false, row, mc)) $(this).css("background", "#ffffff")
        }))
        tr.append(newSpace(pl))
    }
    // Add the ending vertical dash
    tr.append(newVertiLine(pl, ps).click(function(e) {
        handleClick(false, row, width)
    }).hover(function(e) {
        if (!hasBeenPicked(false, row, width)) $(this).css("background", playerColor(currentPlayer))
    }, function(e) {
        if (!hasBeenPicked(false, row, width)) $(this).css("background", "#ffffff")
    }))
    return tr
}

// Resets the table to a width and height
let resetTable = function(width, height) {
    let maxDim = Math.min($("#gamecontainer").width(), $("#gamecontainer").height())
    let ps = maxDim / ((7 * width) + 1)
    let pl = ps * 6
    // Gets table element
    let table = getTable()
    // Empties it
    table.empty()
    // For every row of the game
    for (var row = 0; row < height; row++) {
        // Add the row to the table
        table.append(createPointRow(row, width, ps, pl))
        table.append(createSpaceRow(row, width, ps, pl))
    }
    table.append(createPointRow(height, width, ps, pl))
}

let setupGame = function(width, height, np) {
    let nh = Math.min(width, height)
    let nw = Math.max(width, height)
    resetTable(nw, nh)
    var ta = []
    for (var row = 0; row < nh; row++) {
        var ra = []
        for (var col = 0; col < nw; col++) {
            ra.push(-1);
        }
        var ra2 = []
        for (var col = 0; col <= nw; col++) {
            ra2.push(-1);
        }
        ta.push(ra, ra2);
    }
    var ra = []
    for (var col = 0; col < nw; col++) {
        ra.push(-1);
    }
    ta.push(ra)
    gameState = ta
    currentPlayer = 0
    cNumPlayers = np
}

let handleClick = function(isHoriz, row, col) {
    if (!hasBeenPicked(isHoriz, row, col)) {

        var nextPlayer = true

        let me = [isHoriz, row, col]
        gameState[(row * 2) + (!isHoriz)][col] = currentPlayer
        if (isHoriz) {
            let box1 = [me, [false, row - 1, col + 1],
                [true, row - 1, col],
                [false, row - 1, col]
            ]
            let box2 = [me, [false, row, col],
                [false, row, col + 1],
                [true, row + 1, col]
            ]
            let above = box1.every(function(t) {
                return hasBeenPicked(t[0], t[1], t[2])
            })
            let below = box2.every(function(t) {
                return hasBeenPicked(t[0], t[1], t[2])
            })
            if (above) {
                let spr = row - 1
                let spc = col
                getSpace(spr, spc).css("background", playerColor(currentPlayer))
                nextPlayer = false
            }
            if (below) {
                let spr = row
                let spc = col
                getSpace(spr, spc).css("background", playerColor(currentPlayer))
                nextPlayer = false
            }
        }
        else {
            let box1 = [me, [true, row, col - 1],
                [false, row, col - 1],
                [true, row + 1, col - 1]
            ]
            let box2 = [me, [true, row, col],
                [false, row, col + 1],
                [true, row + 1, col]
            ]
            console.log(box1)
            let left = box1.every(function(t) {
                console.log("Testing " + t[1] + "," + t[2] + " with " + t[0])
                return hasBeenPicked(t[0], t[1], t[2])
            })
            let right = box2.every(function(t) {
                return hasBeenPicked(t[0], t[1], t[2])
            })

            if (left) {
                console.log("HI")
                let spr = row
                let spc = col - 1
                getSpace(spr, spc).css("background", playerColor(currentPlayer))
                nextPlayer = false
            }
            if (right) {
                let spr = row
                let spc = col
                getSpace(spr, spc).css("background", playerColor(currentPlayer))
                nextPlayer = false
            }
        }

        if (nextPlayer) currentPlayer = (currentPlayer + 1) % cNumPlayers
    }
}

let hasBeenPicked = function(isHoriz, row, col) {
    try {
        let ar = gameState[(row * 2) + (!isHoriz)][col]
        return ar != undefined && ar !== -1
    }
    catch (e) {
        return false
    }
}

let playerColor = function(num) {
    return playerColors[num]
}

let getSpace = function(row, col) {
    let table = $("#gametable").find('tr').map(function() {
        return [$("td", this).get()]
    }).get()
    return $(table[(row * 2) + 1][(col * 2) + 1])
}

let showSettings = function() {
    var x = document.getElementById("settings-page");
    if (x.style.display === "none") {
        x.style.display = "block";
    }
    else {
        x.style.display = "none";
    }
}

$("document").ready(function() {
    setupGame(3, 3, 2)
})
